# Troubleshooting Potential Bug in HistFactorySimultaneous with Minos

## Content

The following analysis utilizes modifications of two resources originally sourced from the Root Project on GitHub:
- `example.C` s a modified version of the example available [here](https://github.com/root-project/root/blob/master/tutorials/histfactory/hf001_example.C). The alterations were made in order to carry out a specific fit.
- `prepareHistFactory` has been taken from [here](https://github.com/root-project/root/blob/master/roofit/histfactory/config/prepareHistFactory) and is used to generate the example data for this analysis.

## Issue

The issue lies within the execution of the C++ macro, which can be run using the following command:
```bash
root example.C
```

The conditions leading up to this issue are as follows:
- The overall Probability Density Function (PDF) is constructed using HistFactory.
- The Beeston Barlow Lite method is employed, thus `ActivateStatError()` is used, leading to a series of constrained `gamma_stat` errors.
- `HistFactorySimultaneous` is used (this bug does not occur if HistFactorySimultaneous is not used).

Although the model in this example does not necessitate a simultaneous PDF, it was utilized to quickly illustrate an anomaly we're encountering in our analysis.

The problem arises when using MINOS; the gamma parameter values
`gamma_stat_[...]` are being modified, despite my belief that they should remain constant.



To witness this behavior, after running `root example.C`, you should observe the following:

```
[...]

======== Before MINOS ========
Lumi: 1
SigXsecOverSM: 1.11544
alpha_syst1: 0
alpha_syst2: -0.00946086
alpha_syst3: 0.0209843
gamma_stat_channel1_bin_0: 0.999547
gamma_stat_channel1_bin_1: 1.00348
nom_alpha_syst1: 0
nom_alpha_syst2: 0
nom_alpha_syst3: 0
nom_gamma_stat_channel1_bin_0: 400
nom_gamma_stat_channel1_bin_1: 100

[...]

======== After MINOS ========
Lumi: 1
SigXsecOverSM: 1.11544
alpha_syst1: 0
alpha_syst2: -0.00946086
alpha_syst3: 0.0209843
gamma_stat_channel1_bin_0: 1.02666
gamma_stat_channel1_bin_1: 1.04061
nom_alpha_syst1: 0
nom_alpha_syst2: 0
nom_alpha_syst3: 0
nom_gamma_stat_channel1_bin_0: 400
nom_gamma_stat_channel1_bin_1: 100
nominalLumi: 1
```
(I have tried to set the gamma parameters to constant using `gammaParam.setConstant()`
but this did not work.)

